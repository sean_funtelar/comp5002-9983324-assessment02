
public class Person
{
    int         id = -1;
    string      name = "";
    string      userName = "";

    public int pID          {   get {return id;}        set {id = value;}       }
    public string pName     {   get {return name;}      set {name = value;}     }
    public string pUserName {   get {return userName;}  set {userName = value;} }

    public Person(int _id, string _name)
    {
        id = _id;
        name = _name;
    }
}
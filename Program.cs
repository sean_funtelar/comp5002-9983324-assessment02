﻿using System;
using System.Collections.Generic;

namespace comp5002_9983324_assessment02
{
    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            Console.Clear();

            List<Student>      students = new List<Student>();
            List<Teacher>      teachers = new List<Teacher>();
            List<Subject>      subjects = new List<Subject>();

            // Set up Subjects
            subjects.Add(new Subject("HOON101", "How to really do a gymkhana 101", "Ken Block"));
            subjects.Add(new Subject("TOPGEAR201", "Yes....you can drift a passat 303", "Tanner Foust"));
            subjects.Add(new Subject("TOPGEAR101", "How to get sacked on the biggest motoring show on Earth", "Jeremy Clarkson"));
            subjects.Add(new Subject("MCM101", "Mighty Car Mods", "Moog & Marty"));
            subjects.Add(new Subject("TJ101", "How to waste money on crap mods on cars", "TJ hunt"));

            // Set up Students
            students.Add(new Student("Adam", students.Count, "Adam Lz"));
            students.Add(new Student("Nicole", students.Count, "Nicole Lz"));
            students.Add(new Student("James", students.Count, "James May"));
            students.Add(new Student("Haggard Af", students.Count, "Haggard Af Orion"));
            students.Add(new Student("Casey Niestat", students.Count, "Casey Niestat"));
            // Set up Teachers
            teachers.Add(new Teacher("Ken_Block", teachers.Count, "Ken Block"));
            teachers.Add(new Teacher("Tanner_Foust", teachers.Count, "Tanner Foust"));
            teachers.Add(new Teacher("Jeremy_Clarkson", teachers.Count, "Jeremy Clarkson"));
            teachers.Add(new Teacher("Mood&_Marty", teachers.Count, "Moog & Marty"));
            teachers.Add(new Teacher("TJ_Hunt", teachers.Count, "TJ hunt"));

            // Loop 1 - What subjects are the teachers teaching
            for (int i = 0; i < teachers.Count; i++)
            {
                Console.WriteLine();
                Console.WriteLine(teachers[0].WhatAmITeaching(ref subjects, teachers[i].pName));
                Console.WriteLine();
            }
            
            // Loop 2 - What subjects are students studying
            for (int i = 0; i < students.Count; i++)
            {
                Console.WriteLine();
                Console.WriteLine(students[i].ListAllMySubjects(ref subjects));
                Console.WriteLine();
            }
            
            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }
    }
}
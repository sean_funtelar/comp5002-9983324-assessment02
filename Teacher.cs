using System.Collections.Generic;

public class Teacher : Person
{
    public Teacher(string _userName, int _id, string _name) : base(_id, _name)
    {
        pUserName = _userName;
    }

    public string WhatAmITeaching(ref List<Subject> subjects, string name)
    {
        for (int i = 0; i < subjects.Count; i++)
        {
            if (subjects[i].pTeacher == name)  return $"{name} is teaching {subjects[i].pCourseName}.";
        }
        return $"ERROR: {name} has not been assigned a subject to teach.\n";
    }
}
using System.Collections.Generic;
public class Student : Person
{
    public Student(string _userName, int _id, string _name) : base(_id, _name)
    {
        pUserName = _userName;
    }

    public string ListAllMySubjects(ref List<Subject> subjects)
    {
        //string result = $"{pName} is studying: \n";
        System.Text.StringBuilder sb = new System.Text.StringBuilder($"{pName} is studying: \n");
        for (int i = 0; i < subjects.Count; i++)
        {
            sb.Append(subjects[i].pCourseName + "\n");
            //string.Concat(result + subjects[i].pCourseName + "\n");
        }
        return sb.ToString();
    }
}
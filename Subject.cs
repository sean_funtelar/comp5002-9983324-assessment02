
public class Subject
{
    string      courseCode = "";
    string      courseName = "";
    string      teacher = "";

    public string pCourseCode       {   get {return courseCode;}    set {courseCode = value;}   }
    public string pCourseName       {   get {return courseName;}    set {courseName = value;}   }
    public string pTeacher          {   get {return teacher;}       set {teacher = value;}      }

    public Subject(string _courseCode, string _courseName, string _teacher)
    {
        courseCode  = _courseCode;
        courseName  = _courseName;
        teacher     = _teacher;
    }
}